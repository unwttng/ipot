"""
Provides the entire thermostat code, monitoring a one-wire Dallas temperature
sensor and driving a heater from the results.
"""

import json
import signal
import os
from time import sleep, time
from copy import deepcopy

import arrow
from w1thermsensor import W1ThermSensor
import requests
import RPi.GPIO as GPIO  # pylint: disable=E0401

LOOP_PERIOD = 1

SENSOR_RESOLUTION = 12

MIN_TEMPERATURE = 19
MAX_TEMPERATURE = 21

HEATER_PIN = 17

HEATER_ON_PIN = 14
HEATER_OFF_PIN = 15
HEATER_SWITCH_PULSE_LENGTH = 5

OPENTSDB_DEVICE_ID = os.environ.get('OPENTSDB_DEVICE_ID')
OPENTSDB_URL = os.environ.get('OPENTSDB_URL')
OPENTSDB_GATEKEEPER_SECRET = os.environ.get('OPENTSDB_GATEKEEPER_SECRET')

def log(log_str):
    "Log the given string with useful time info"
    gmt = arrow.utcnow().to('Europe/London')
    print '{} ({}): {}'.format(
        gmt.timestamp,
        gmt.format('YYYY-MM-DD HH:mm:ss ZZ'),
        log_str
    )

def send_metric(metric, value, tags={}):  # pylint: disable=W0102
    "Send a metric to OpenTSDB if required environment variables were given."
    if (not OPENTSDB_DEVICE_ID) or (not OPENTSDB_URL):
        return
    extended_tags = deepcopy(tags)
    extended_tags['deviceid'] = OPENTSDB_DEVICE_ID
    data = {
        'metric': 'ipot.{}'.format(metric),
        'value': value,
        'timestamp': int(time()) * 1000,
        'tags': extended_tags
    }
    if OPENTSDB_GATEKEEPER_SECRET:
        data['secret'] = OPENTSDB_GATEKEEPER_SECRET
    try:
        requests.post(OPENTSDB_URL, json=data)
    except requests.exceptions.HTTPError as e:
        log('HTTPError talking to {} with data {}'.format(
            OPENTSDB_URL,
            json.dumps(data, sort_keys=True)
        ))
    except requests.exceptions.ConnectionError:
        log('ConnectionError talking to {} with data {}'.format(
            OPENTSDB_URL,
            json.dumps(data, sort_keys=True)
        ))

def setup_gpio():
    "Setup GPIO mode and various used pins."
    log('Setting up GPIO')
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(HEATER_PIN, GPIO.OUT)
    GPIO.setup(HEATER_ON_PIN, GPIO.OUT)
    GPIO.setup(HEATER_OFF_PIN, GPIO.OUT)
    log('Finished setting up GPIO')

def setup_heater():
    """
    Make sure the heater is in the desired starting state (off).

    Return: final state of the heater.
    """
    heater_state = stop_heater()
    actuate_heater(heater_state)
    return heater_state

def get_average_temperature():
    """
    Get the average temperature from all connected one-wire temperature sensors.

    Raises: NoSensorsDetectedException if no sensors detected.
    """
    temps = [
        s.get_temperature(W1ThermSensor.DEGREES_C)
        for s in W1ThermSensor.get_available_sensors()
    ]
    if not temps:
        raise NoSensorsDetectedException()
    average = (sum(temps) / float(len(temps)))
    log('Read {} sensors for an average of {}C : {}'.format(len(temps), average, temps))
    send_metric('temperature.avg', average)
    return average

def start_heater():
    """
    Set the program heater state and switch it on physically.

    Return: new heater state as on / off bool.
    """
    log('Starting heater')
    actuate_heater(True)
    return True

def stop_heater():
    """
    Set the program heater state and switch it off physically.

    Return: new heater state as on / off bool.
    """
    log('Stopping heater')
    actuate_heater(False)
    return False

def actuate_heater(heater_state):
    "Turn the heater on or off dependent on current heater state in-program."
    GPIO.output(HEATER_PIN, heater_state)
    heater_actuate_pin = HEATER_ON_PIN if heater_state else HEATER_OFF_PIN
    GPIO.output(heater_actuate_pin, True)
    sleep(HEATER_SWITCH_PULSE_LENGTH)
    GPIO.output(heater_actuate_pin, False)

def init():
    """
    Initialise the program, GPIO and state.

    Returns: initial program state.
    """
    state = {}
    setup_gpio()
    state['heater'] = setup_heater()
    return state

def loop(state):
    """
    Perform a single "clock" of the main run loop.

    Returns: new state object.
    """
    send_metric('heater.on', 1 if state['heater'] else 0)

    new_state = deepcopy(state)
    average_temperature = get_average_temperature()

    if average_temperature <= MIN_TEMPERATURE and not state['heater']:
        new_state['heater'] = start_heater()
    elif average_temperature >= MAX_TEMPERATURE and state['heater']:
        new_state['heater'] = stop_heater()

    return new_state

def boot():
    "Perform initialisation and then go through main run loop until stopped."

    def kill_with_signal(signum, frame):  # pylint: disable=W0613
        "Perform action in response to a signal kill."
        raise SignalKilledException()

    signal.signal(signal.SIGINT, kill_with_signal)
    signal.signal(signal.SIGTERM, kill_with_signal)

    state = init()
    while True:
        state = loop(state)
        sleep(LOOP_PERIOD)

def cleanup():
    "Performed on end of execution / interrupt - switch off the heater and free up GPIO pins."
    log('Cleaning up for exit')
    stop_heater()
    GPIO.cleanup()

class NoSensorsDetectedException(Exception):
    "No one-wire sensors were detected when some were expected."
    pass

class SignalKilledException(Exception):
    "An external signal attempted to kill execution."
    pass

if __name__ == '__main__':
    try:
        boot()
    except SignalKilledException:
        log('Signal kill')
        cleanup()
    except KeyboardInterrupt:
        log('Keyboard interrupt')
        cleanup()
